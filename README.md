# infinity-test

#### Prerequisites:
- Docker:  <https://docs.docker.com/docker-for-mac/install/>
- AWS profile (with admin access)

#### Installation:
    Run the following commands in order
  
- `make docker-build`  - This command will create a docker image with php, terraform, sam, etc, that we will use for our deployment

- `make aws_config` - This command will cat your ~/.aws/credentials so you can easily copy the credentials ('access_key, secret_key' ) of the profile with admin access, so we can create a default profile inside the container

- `make deploy` -  This command will deploy the stack to AWS

`Once it's deployed you will see in the outputs the apigateway url and the api-key`   
 This is not secure but in this case I think is ok.

`make db_seed` - Populate dynamodb table with some fake data to test the API

#### Test The API
- Replace the API key and the apigateway endpoint below with the one from terraform output  

##### Retrieve data
- Get all users from china  
``curl -X POST -H "x-api-key: lMupclqman3lR5GhUBgZc8TzFsXg7DMZ3qICz4VV" -H "Content-Type: application/json" -d '{"country":"China"}' https://o1cydx1dj8.execute-api.us-east-1.amazonaws.com/v1/getuser | json_pp``  

- Get user by phone number  
``curl -X POST -H "x-api-key: lMupclqman3lR5GhUBgZc8TzFsXg7DMZ3qICz4VV" -H "Content-Type: application/json" -d '{"phone":"+86 231 361 8930"}' https://o1cydx1dj8.execute-api.us-east-1.amazonaws.com/v1/getuser | json_pp``

##### Put data
``curl -X POST -H "x-api-key: lMupclqman3lR5GhUBgZc8TzFsXg7DMZ3qICz4VV" -H "Content-Type: application/json" -d '{"last_name": "Simpson","company": "The Simpsons","ip_address": "200.169.221.10","first_name": "Homer","email": "homer@simpson.com","country": "United States","phone": "+1 555-8707"}' https://o1cydx1dj8.execute-api.us-east-1.amazonaws.com/v1/putuser | json_pp``

#### Clean up
Once you're done you can easily remove the stack from AWS as well as docker used for this test by running the command below
 - `make destroy && make docker-down`   

