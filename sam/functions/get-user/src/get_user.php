<?php

use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;

function get_user($data)
{
    $sdk = new Aws\Sdk(
        [
            'region' => 'us-east-1',
            'version' => 'latest'
        ]
    );

    if (empty($data)) {
        return APIResponse([], 204);
    }

    $dynamodb = $sdk->createDynamoDb();
    $marshaler = new Marshaler();
    $tableName = $_ENV['UsersDDBTable'];
    $params = [
        "TableName" => $tableName
    ];

    if (isset($data['country']) && !empty($data['country'])) {
        $params["IndexName"] = "country-index";
    }

    if (isset($params["IndexName"]) && $params["IndexName"] == "country-index") {
        $params["KeyConditions"] = [
            "country" => [
                "ComparisonOperator" => "EQ",
                "AttributeValueList" => array(array("S" => ucfirst($data['country'])))
            ]
        ];
    }
    else {
        $params["KeyConditions"] = [
            "phone" => [
                "ComparisonOperator" => "EQ",
                "AttributeValueList" => array(array("S" => $data['phone']))
            ]
        ];
    }


    try
    {
        $users = [];
        $result = $dynamodb->query($params);
        echo "Query succeeded.\n";
        if (!empty($result)) {
            foreach ($result['Items'] as $i) {
                $user = $marshaler->unmarshalItem($i);
                $users[] = $user;
            }
            return APIResponse($users);
        }
    } catch (DynamoDbException $e) {
        echo "Unable to query:\n";
        echo $e->getMessage() . "\n";
    }
}

function APIResponse($body, $statusCode = 200) {
    return json_encode(array(
        "statusCode" => $statusCode,
        "body" => $body
    ));
}
