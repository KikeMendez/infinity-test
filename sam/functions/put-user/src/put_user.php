<?php

use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;

function put_user($data)
{
    $response = [];
    $sdk = new Aws\Sdk(
        [
            'region' => 'us-east-1',
            'version' => 'latest'
        ]
    );

    if (empty($data)) {
        return APIResponse([], 204);
    }

    $dynamodb = $sdk->createDynamoDb();
    $marshaler = new Marshaler();
    $tableName = $_ENV['UsersDDBTable'];

    // Used to check if the fields coming to insert are correct
    $validFields = [
        'first_name', 'last_name', 'phone', 'email', 'ip_address', 'company', 'country'
    ];

    $valid = true;
    foreach ($data as $key => $value) {
        if (!in_array($key, $validFields) || empty($value)) {
            $valid = false;
        }
    }

    if ($valid) {
        $user = json_encode([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'ip_address' => $data['ip_address'],
            'company' => $data['company'],
            'country' => $data['country']
        ]);

        $params = [
            'TableName' => $tableName,
            'Item' => $marshaler->marshalJson($user)
        ];

        try
        {
            $result = $dynamodb->putItem($params);
            $response['message'] = "user: " . $data['first_name'] . " " . $data['phone'] . " added successfully";
            return APIResponse($response, 201);
        } catch (DynamoDbException $e)
        {
            echo "Unable to add user:\n";
            $response['message'] = "user could not be added!";
            echo $e->getMessage() . "\n";
            return APIResponse($response,409);
        }
    }

    $response["message"] = "make sure all fields are present and the value is not empty!";
    $response["valid_fields"] = $validFields;
    return APIResponse($response,200);
}

function APIResponse($body, $statusCode = 200) {
    return json_encode(array(
        "statusCode" => $statusCode,
        "body" => $body
    ));
}