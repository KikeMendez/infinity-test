<?php

require '../vendor/autoload.php';

date_default_timezone_set('UTC');

use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;

$sdk = new Aws\Sdk([
    'region'   => 'us-east-1',
    'version'  => 'latest'
]);

$dynamodb = $sdk->createDynamoDb();
$marshaler = new Marshaler();
$tableName = 'infinity-test-enrique-mendez-table';

$items = json_decode(file_get_contents('dynamodb_sample_data.json'), true);

foreach ($items as $item) {
    $json = json_encode([
        'first_name' => $item['first_name'],
        'last_name' =>  $item['last_name'],
        'phone' =>  $item['phone'],
        'email' => $item['email'],
        'ip_address' => $item['ip_address'],
        'company' => $item['company'],
        'country' => $item['country']
    ]);

    $params = [
        'TableName' => $tableName,
        'Item' => $marshaler->marshalJson($json)
    ];

    try {
        $result = $dynamodb->putItem($params);
        echo "Added user: " . $item['first_name'] . " " . $item['phone'] . " " . $item['country'] . "\n";
    } catch (DynamoDbException $e) {
        echo "Unable to add user:\n";
        echo $e->getMessage() . "\n";
        break;
    }
}
