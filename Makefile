docker-build:
	docker-compose up -d --force-recreate  --build infinity-test-enrique-mendez && \
	make install_dependencies

docker-down:
	docker-compose down --rmi 'all'

docker-start:
	docker-compose start

docker-stop:
	docker-compose stop

sam_deployment_bucket:
	docker-compose exec infinity-test-enrique-mendez sh -c "aws s3api create-bucket --bucket infinity-test-enrique-mendez-sam-templates --region us-east-1"

install_dependencies:
	docker-compose exec infinity-test-enrique-mendez sh -c "composer install"

db_seed:
	docker-compose exec infinity-test-enrique-mendez sh -c "cd dynamodb_data && php dynamodb_populate.php"

aws_config:
	cat ~/.aws/credentials && echo "\n" && \
	docker-compose exec infinity-test-enrique-mendez aws configure --profile=default
	chmod 666 ~/.aws/credentials

package_layer:
	docker-compose exec infinity-test-enrique-mendez sh -c "zip -r vendor.zip vendor && cd runtime && zip -r runtime.zip bin bootstrap"

clean_package_layer:
	docker-compose exec infinity-test-enrique-mendez sh -c "rm vendor.zip && rm runtime/runtime.zip"

sam_package:
	make package_layer && \
	docker-compose exec infinity-test-enrique-mendez sh -c "cd sam && sam package --template-file sam-template.yaml --output-template-file sam-deploy.yaml --s3-bucket infinity-test-enrique-mendez-sam-templates"

deploy:
	make sam_deployment_bucket && \
	make sam_package && \
	docker-compose exec infinity-test-enrique-mendez sh -c "cd terraform && terraform init && terraform apply -auto-approve"

destroy:
	docker-compose exec infinity-test-enrique-mendez sh -c "cd terraform && terraform destroy -auto-approve" && \
	docker-compose exec infinity-test-enrique-mendez sh -c " aws s3 rm s3://infinity-test-enrique-mendez-sam-templates --recursive --region us-east-1" && \
	docker-compose exec infinity-test-enrique-mendez sh -c "aws s3api delete-bucket --bucket infinity-test-enrique-mendez-sam-templates --region us-east-1" && \
	make clean_package_layer


# sam package --template-file sam-template.yaml --output-template-file sam-deploy.yml --s3-bucket kike-deploy-test-001